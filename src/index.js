//[SECTION] React Dependencies/Modules
  import React from 'react';
  import ReactDOM from 'react-dom/client';
  import App from './App';
  import 'bootstrap/dist/css/bootstrap.min.css';

//REACT FRAGMENT = <React.Fragment>....</React.Fragment> shortcut => <>...</>
  const root = ReactDOM.createRoot(document.getElementById('root'));
  root.render(
      <App />
  );
