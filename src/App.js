//[SECTION] React Components
  import { useState } from 'react';
  import Styles from './Styles.css';
  import AppNavbar from './components/AppNavbar';
  import Home from './pages/Home';
  import ProductCatalog from './pages/ProductCatalog';
  import Register from './pages/Register';
  import Login from './pages/Login';
  import Logout from './pages/Logout';
  import OrderHistory from './pages/OrderHistory';
  import ErrorPage from './pages/ErrorPage';
  import SpecificProduct from './pages/SpecificProduct';
  import Cart from './pages/Cart';
  import { UserProvider } from './UserContext';
  
  
//[SECTION] REACT-ROUTER
  import { BrowserRouter, Routes, Route } from 'react-router-dom';

  
function App() {
  const [user, setUser] = useState({
      accessToken: localStorage.getItem('accessToken'),
      isAdmin: localStorage.getItem('isAdmin') === 'true',
      regularUser: localStorage.getItem('regularUser')
    })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
          <AppNavbar />
              <Routes>
                  <Route path="/" element={ <Home /> }/>
                  <Route path="/products" element={ <ProductCatalog /> }/>  
                  <Route path="/register" element={ <Register /> }/>
                  <Route path="/login" element={ <Login /> }/>  
                  <Route path="/logout" element={ <Logout /> }/> 
                  <Route path="/products/:productId" element={ <SpecificProduct /> }/>
                  <Route path="/myCartView" element={ <Cart /> }/> 
                  <Route path="/orderHistory" element={ <OrderHistory /> }/> 
                  <Route path="*" element={ <ErrorPage /> }/> 
              </Routes>
      </BrowserRouter> 
    </UserProvider>
  );
}

export default App;
