//[SECTION] React Components
	import React from 'react';

const UserContext = React.createContext();

//Provider component 
export const UserProvider = UserContext.Provider;

export default UserContext;