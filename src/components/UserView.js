//[SECTION] React Components
	import React, { useState, useEffect } from 'react';
	import ProductCard from './ProductCard';
	import { Row, FormControl, Form, Button } from 'react-bootstrap';


export default function UserView({productsData}) {

	const [ products, setProducts ] = useState([])

	useEffect(() => {
		const productsArr = productsData.map(product => {
			//Only render the *active* products
			if(product.isActive === true) {
				return(
					<ProductCard data={product} key={product._id} breakPoint={3}/>
					)
			} else {
				return null;
			}
		})
		setProducts(productsArr)

	}, [productsData])

	return(
		<>
			<Row>
				<Form className="d-flex" style={{ width: '18rem'}}>
					<FormControl
						type="search"
						placeholder="Search"
						className="me-2"
						aria-label="Search"
						    />
					<Button style={{backgroundColor: "#E8998D"}} variant="light" className="text-white"><i class="bi bi-search"></i></Button>
				</Form>
			</Row>

			<Row>
			<h2 className="text-center mb-4" style={{color: "#231942"}}>Skin Care Collection</h2>
				{ products }
			</Row>
		</>
		)
}