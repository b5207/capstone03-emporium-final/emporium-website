//[SECTION] React Bootstrap Components
	import { Button, Row, Col } from 'react-bootstrap';
	import { Link } from 'react-router-dom';
	import { Container } from 'react-bootstrap';
	import Styles from '../Styles.css';
 	import "bootstrap/dist/css/bootstrap.min.css";


export default function Banner(props) {
	return (
		
 	<>
 	<Container className="container">
		<Row>
			<Col className="p-3">
				<h1 variant="light" style={{ color:"#5E548E" }}>YOUR SKIN IS LUXURY. SO GIVE YOUR SKIN</h1>
				<h1 variant="light" style={{ color:"#5E548E" }}>THE RIGHT CARE IT DESERVES.</h1>
				<p className="mb-3">Here at Emporium, we believe skin care is the best gift we can give ourselves. If you're going to spend your money on skin care products, then spend it on premium products that will give you the best results and are fit for your skin type. Whatever product you are looking for, we have it for you.</p>
				<Button variant="light" style={{ backgroundColor: "#E8998D", color:"#fff" }} as={ Link } to="/products">Shop Products</Button>
			</Col>
		</Row>
	</Container>
	</>	
		)
}