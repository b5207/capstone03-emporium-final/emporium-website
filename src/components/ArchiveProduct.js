//[SECTION] React Components
	import { Button } from 'react-bootstrap';
	import Swal from 'sweetalert2';

 
export default function ArchiveProduct({ product, isActive, fetchData }){

	const archiveToggle = (productId) => {
		fetch(`https://emporium-global.herokuapp.com/products/${ productId }/archive`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data !== false) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product has been archived.'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Product was not archived. Try again.'
				})
				fetchData()
			}
		})
	}


	//For activating the course
	const activateToggle = (productId) => {
		fetch(`https://emporium-global.herokuapp.com/products/${productId}/activate`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data !== false) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product has been activated.'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Product was not activated. Try again.'
				})
				fetchData()
			}
		})
	}


	return(
		<>
			{isActive ?
				<Button variant="light" style={{backgroundColor: "#231942" , color: "#fff"}} size="sm" onClick={() => archiveToggle (product)}>Disable</Button>
				:
				<Button variant="light" style={{backgroundColor: "#6C9A8B" , color: "#fff"}} size="sm" onClick={() => activateToggle(product)}>Enable</Button>
			}
		</>
		)
}