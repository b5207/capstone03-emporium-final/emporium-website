//[SECTION] React Components
	import { useState } from 'react';
	import { Button, Modal, Form } from 'react-bootstrap';
	import Swal from 'sweetalert2';


export default function AddProduct({fetchData}) {

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ image, setImage ] = useState();

	//States for opening and closing the modals
	const [ showAdd, setShowAdd ] = useState(false);

	//Functions to handle opening and closing of our Modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	const addProduct = (e) => {
		e.preventDefault();

		let formData = new FormData()
    	formData.append('name', name)
    	formData.append('description', description)
    	formData.append('price', price)
    	formData.append('image', image)
    	

		fetch('https://emporium-global.herokuapp.com/products/create', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: formData
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
					title: 'Add Product Status',
					icon: 'success',
					text: 'Product has been successfully added.'
				})
				closeAdd()
				fetchData()

			} else {
				Swal.fire({
					title: 'Add Product Status',
					icon: 'error',
					text: 'Product was not added. Try again.'
				})
				fetchData()
			}
			//Reset all states input
			setName('');
			setDescription('');
			setPrice(0);
			setImage('');
		})
	}

	return (
		<>
			<Button variant="light" style={{backgroundColor: "#E8998D" , color: "#fff"}} onClick={openAdd}>Add New Product</Button>
			
			{/*Add modal*/}

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control
                                  type="text"
                                  required
                                  value={name}
                                  onChange={e => setName(e.target.value)}
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control
                                  type="text"
                                  required
                                  value={description}
                                  onChange={e => setDescription(e.target.value)}
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control
                                  type="number"
                                  required
                                  value={price}
                                  onChange={e => setPrice(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="formFile" className="mb-3">
						    <Form.Label>Upload Image</Form.Label>
						    <Form.Control 
						    type="file" 
						    required
						    onChange={e => setImage(e.target.files[0])}
						    />
						</Form.Group>

					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>

				</Form>
			</Modal>
		</>



		)
}