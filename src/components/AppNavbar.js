//[SECTION] React Components
	import { Link } from 'react-router-dom';
	import { useState, useContext, useEffect } from 'react';
	import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
	import UserContext from '../UserContext';
  	import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
  	import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
	import { FaUserCircle } from "react-icons/fa";
	//import UserSolidSvg from "../assets/svgs/user-solid.svg";
	

export default function AppNavbar() {

	const { user } = useContext(UserContext);
	const [name, setName] = useState('');

	useEffect(() => {
			fetch('https://emporium-global.herokuapp.com/users/details', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				let name = data.email.split('@')
				setName(name[0].charAt(0).toUpperCase() + name[0].slice(1))
			})
			setName(name)
		}, [name])

	return(

		<Navbar expand="lg" variant="dark" className="color-nav">
			<Navbar.Brand className="ms-3" as={Link} to="/home">Emporium</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">

				<Nav className="ms-auto d-flex flex-row">
					{(user.accessToken !== null) ?
						<>
						<NavDropdown as={Link} to="/" eventKey={3} title={<div style={{display:"inline-block", color:"#fff"}}> 
						<FaUserCircle />  {`${name}`}</div>} id="basic-nav-dropdown">
							<NavDropdown.Item eventKey={3.1} as={Link} to="/logout">Logout</NavDropdown.Item>
						</NavDropdown>
						</>
						:
						<>
							<NavDropdown eventKey={3} title={<div style={{display: "inline-block", color:"#fff"}}>  <FaUserCircle /> </div>} id="basic-nav-dropdown">
							    <NavDropdown.Item eventKey={3.1} as={Link} to="/login">Login</NavDropdown.Item>
							    <NavDropdown.Item eventKey={3.2} as={Link} to="/register">Sign Up</NavDropdown.Item>
							</NavDropdown>	
						</>
					}
					<Nav.Link as={Link} to="/products" style={{display:"inline-block", color:"#fff"}}>Shop</Nav.Link>
					
					<Nav.Link as={Link} to="/myCartView" className="text-white" style={{display:"inline-block", color:"#fff"}}>
						<FontAwesomeIcon icon={faCartShopping} />
					</Nav.Link>	
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
}