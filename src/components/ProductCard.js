//[SECTION] React Components
	import React from 'react';
	import { useState, useEffect } from 'react';
	import { Card, Button, Row, Col } from 'react-bootstrap';
	import PropTypes from 'prop-types';
	import { Link } from 'react-router-dom';

	

export default function ProductCard(props){
	const { breakPoint, data } = props;

	const { _id, name, description, price, image } = data;
	
	return(
		
            <Col xs={12} md={breakPoint}>
	            <Card className="mt-3" style={{ width: '18rem' }}>
	            	<Card.Img variant="top" src={image}/>
	            	
					<Card.Body>
						<Card.Title className="text-center" style={{color: "#5E548E"}}>{ name }</Card.Title>
						
						<Card.Text>{ description }</Card.Text>

						<h6 style={{color: "#5E548E"}}><span>&#8369;</span>{ price }</h6>

						<Button style={{backgroundColor: "#E8998D", color: "#fff"}} variant="light" as={ Link } to={`/products/${_id}`}>Details
						</Button>
					</Card.Body>
				</Card>
			</Col>
			
		);
};


	ProductCard.propTypes = {

		prodProp: PropTypes.shape({
			//Define the properties and their expected types.
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.number.isRequired,
			image: PropTypes.string.isRequired
		})
	}