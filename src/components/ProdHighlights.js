//[SECTION] React Bootstrap Components
	import { Row, Col, Card } from 'react-bootstrap';
	import Styles from '../Styles.css';
	import { Container } from 'react-bootstrap';

export default function ProdHighlights(){
	return(
		<>
		<Container className="container">
			<Row className="mt-4">
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					 <Card.Img variant="top" src={require("../productPhotos/12.jpg")} />
						<Card.Body>
							<Card.Title>
								<h2>Night Complex Package 1</h2>
							</Card.Title>

							<Card.Text>
								Treat yourself because you deserve it. Nourish and hydrate your skin with a wide range of our night complex products. 
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					<Card.Img variant="top" src={require("../productPhotos/128.jpg")} />
						<Card.Body>
							<Card.Title>
								<h2>Night Cosmetics Luxury Collection</h2>
							</Card.Title>

							<Card.Text>
								You are luxury, so treat yourself extravagantly as you should. Nourish and hydrate your skin with a wide range of night cosmetic products.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					<Card.Img variant="top" src={require("../productPhotos/56.jpg")} />
						<Card.Body>
							<Card.Title>
								<h2>Night Complex Package 2</h2>
							</Card.Title>

							<Card.Text>
								Treat yourself because you deserve it. Nourish and hydrate your skin with a wide range of our night complex products.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		</>
		)
}