//[SECTION] React Components
	import UserView from '../components/UserView';
	import AdminView from '../components/AdminView';
	import { useContext, useEffect, useState } from 'react';
	import UserContext from '../UserContext';
	import { Card, Button, Row, Col } from 'react-bootstrap';
	import Styles from '../Styles.css';
	import { Container } from 'react-bootstrap';


export default function ProductCatalog() {
	const [ allProducts, setAllProducts ] = useState([])

	const fetchData = () => {
		fetch('https://emporium-global.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setAllProducts(data)
		})
	}
	
	//it renders the function fetchData() => it gets the updated data coming from the fetch
	useEffect(() => {
		fetchData()
	}, [])
	//if the useEffect has no variables, it will only render one time.


	const { user } = useContext(UserContext);

	return (
			<Container className="container">
			<h1 className="mt-5" style={{color: "#231942"}}>PRODUCTS</h1>
			{(user.isAdmin === true) ?
				<AdminView productsData={allProducts} fetchData={fetchData}/>
				:
				<UserView productsData={allProducts}/>
			}
			</Container>
		)
} 