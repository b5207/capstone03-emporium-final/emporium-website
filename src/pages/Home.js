//[SECTION] React Components
  import Banner from '../components/Banner';
  import ProdHighlights from '../components/ProdHighlights';
  import {Carousel} from 'react-bootstrap';
  import Styles from '../Styles.css';


export default function Home() {
  
  return(
    <>
      <Carousel variant="dark">
        <Carousel.Item interval={2000} className="h-20">
          <img
            className="d-block w-100"
            src={require("../productPhotos/carousel1.png")}
            alt="First slide"
          />
        </Carousel.Item>

        <Carousel.Item interval={2000} className="h-20">
          <img
            className="d-block w-100"
            src={require("../productPhotos/carousel2.png")}
            alt="Second slide"
          />
        </Carousel.Item>

        <Carousel.Item interval={2000} className="h-20">
          <img
            className="d-block w-100"
            src={require("../productPhotos/carousel3.png")}
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>

      <Banner/>
      <ProdHighlights />
    </>
    )
}