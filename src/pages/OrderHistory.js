//[SECTION] React Components
	import React, { useState, useEffect, useContext } from 'react';
	import { Row, Col, Table } from 'react-bootstrap';
	import UserContext from '../UserContext';
	import {Navigate} from 'react-router-dom';
	import Styles from '../Styles.css';
	import { Container } from 'react-bootstrap';


export default function OrderHistory() {

	const [ myOrders, setMyOrders ] = useState([]);
	const [ orderHistory, setOrderHistory ] = useState([]);

	useEffect(() => {
	
	fetch('https://emporium-global.herokuapp.com/orders/userOrders', {
		headers: { 
			'Content-Type': 'application/json', 
			Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
		}
	})
	.then(response => response.json())
	.then(data => {
		setMyOrders(data)
	})

	const ordersArr = myOrders.map(order => {
			let tempArray = [];
			for(let i = 0; i < order.products.length; i++){
				tempArray.push(` ${order.products[i].prodName} - Quantity: ${order.products[i].quantity} | `)
			}
			let orderedProducts = tempArray.join('\r\n');
			return(
				<tr key={order._id} >
					<td style={{color: "#6C9A8B", fontWeight: 'bold'}}> {order._id}</td>
					<td className="bg-light text-dark">
					  {orderedProducts}
					</td>
					<td style={{color: "#E8998D", fontWeight: 'bold'}}><span>&#8369;</span> {order.totalAmount}</td>
					<td className="bg-light text-dark">{order.purchasedOn}</td>
				</tr>
				)
		})
		setOrderHistory(ordersArr)
	//
	}, [myOrders])


	return(
		<Container className="container">
			<div className="my-4">
				<h1>Order History</h1>
			</div>
			
			<Table striped bordered hover responsive className="text-center">
				<thead className="text-light" style={{backgroundColor: "#5E548E", color: "#fff"}}>
					<tr>
						<th>ORDER ID</th>
						<th>PRODUCTS</th>
						<th>TOTAL AMOUNT</th>
						<th>DATE OF PURCHASE</th>
					</tr>
				</thead>

				<tbody>
					{ orderHistory }
				</tbody>
			</Table>
		</Container>
		)
}