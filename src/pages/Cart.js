//[SECTION] React Components
	import { useState, useEffect } from 'react';
	import { Form, Table, Button, TextField } from 'react-bootstrap';
	import { useNavigate, Link } from 'react-router-dom';
	import Swal from 'sweetalert2';
	import { BsFillCartFill, BsArrowLeft } from "react-icons/bs";
  	import Styles from '../Styles.css';
	import { Container } from 'react-bootstrap';


export default function Cart() {

	const [cart, setCart] = useState([]);
	const [quantity, setQuantity] = useState('');
	const [name, setName] = useState('');
	const [grandTotal, setGrandTotal] = useState(0);
	const [storedData, setStoredData] = useState(JSON.parse(localStorage.getItem('cartitems')));
	const navigate = useNavigate();

	useEffect(() => {
		//[increaseQuantity function]
				function increaseQuantity (prodId) {
					let updatedCartArray = [];
					for (let i = 0; i < storedData.length ; i++) {
						if (storedData[i].productId === prodId){
							storedData[i].quantity += 1;
							storedData[i].subTotal = storedData[i].quantity * storedData[i].price
						}
						updatedCartArray.push(storedData[i])
						
					}
					localStorage.setItem('cartitems', JSON.stringify(updatedCartArray));
					window.location.reload(false);
				}
		//[decreaseQuantity function]
				function decreaseQuantity (prodId) {
					let updatedCartArray = [];
					for (let i = 0; i < storedData.length ; i++) {
						if (storedData[i].productId === prodId){
							if(storedData[i].quantity === 1) {
								storedData[i].quantity = 1
							} else {
								storedData[i].quantity -= 1;
								storedData[i].subTotal = storedData[i].quantity * storedData[i].price
							}
						}
						updatedCartArray.push(storedData[i])
					}
					localStorage.setItem('cartitems', JSON.stringify(updatedCartArray));
					window.location.reload(false);
				}
		//[removeAnItem Function]
				function removeItem (prodId) {
					let updatedCartArray = [];
					for (let i = 0; i < storedData.length ; i++) {
						if (storedData[i].productId === prodId){
								continue;
							}
						updatedCartArray.push(storedData[i])
					}
					localStorage.setItem('cartitems', JSON.stringify(updatedCartArray));
					window.location.reload(false);
				}

		if(localStorage.getItem('cartitems') == null){
			localStorage.setItem('cartitems', '[]')
			Swal.fire({
				title: 'Your cart is empty. Continue shopping?',
				icon: 'question',
				confirmButtonColor: "#B392AC",
			})

			navigate('/products')
		} else {
			const cartArr = storedData.map(cartitem => {
					setGrandTotal(prevGrandTotal => prevGrandTotal + cartitem.subTotal)
					return(
						<tr key={cartitem.productId}>
							<td  Colspan="8">{cartitem.name}</td>
							<td><span>&#8369;</span>{cartitem.price}</td>
							<td>
							<Button className="mx-2" style={{backgroundColor: "#E8998D", color: "#fff"}} variant="light" onClick={() => decreaseQuantity(cartitem.productId)}> - </Button>
									 <span> {` ${cartitem.quantity} `} </span>
							<Button className="mx-2" style={{backgroundColor: "#E8998D", color: "#fff"}} variant="light" onClick={() => increaseQuantity(cartitem.productId)}> + </Button>

							</td>
							<td><span>&#8369;</span>{cartitem.subTotal}
							</td>
							<td>
							<Button className="mx-2" style={{backgroundColor: "#231942", color: "#fff"}} variant="light" onClick={() => removeItem(cartitem.productId)}> Remove </Button>
							</td>

						</tr>
						)
			})
			setStoredData(JSON.parse(localStorage.getItem('cartitems')))
			setCart(cartArr)
		}
	}, [quantity])

	//[ADD TO ORDER FUNCTION]
			const addToOrders = () => {
					if(storedData.length === 0) {
							Swal.fire({
							title: `You don't have any orders yet. Continue shopping?`,
							icon: 'question',
							confirmButtonColor: "#B392AC",
						})
					} else {

						const newOrder = {
							products: storedData,
							totalAmount: grandTotal
						}

						console.log(storedData)

						fetch('https://emporium-global.herokuapp.com/orders/createOrder', {
							method: 'POST',
							headers: {
								'Content-Type': 'application/json',
								Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
							},
							body: JSON.stringify(newOrder)
						})
						.then(res => res.json())
						.then(data => {
							if(data){
								Swal.fire({
								  position: 'top-end',
								  icon: 'success',
								  title: `You have successfully checkout all your cart items!`,
								  confirmButtonColor: "#B392AC",
								  showConfirmButton: false,
								  timer: 3000
								})
								localStorage.removeItem('cartitems');
								navigate('/orderHistory')
							} else {
								Swal.fire({
									title: 'error!',
									icon: 'error',
									confirmButtonColor: "#B392AC",
									text: 'Something went wrong. Please try again.'
								})
							}
						})	
					
				}
			}


	return(
		
		<Container className="container">
			<div>
				<h1 className="mt-5" style={{color: "#231942"}}> {`MY CART`} <BsFillCartFill /></h1>
			</div>
			<Button style={{backgroundColor: "#5E548E" , color: "#fff"}} className="mb-3" variant="light" as={Link} to="/products"> {<BsArrowLeft />} </Button>

			<Table striped bordered hover responsive className="text-center">
				<thead className="text-light" style={{backgroundColor: "#5E548E", color: "#fff"}}>
					<tr>
						<th Colspan="8">{`PRODUCT NAME`}</th>
						<th>{`PRICE`}</th>
						<th>{`QUANTITY`}</th>
						<th>{`SUBTOTAL`}</th>
						<th>{`ACTION`}</th>
					</tr>
				</thead>

				<tbody>
					{ cart }
				</tbody>
			</Table>
			<Button className="m-2" as={Link} to="/orderHistory" style={{backgroundColor: "#6C9A8B", color: "#fff", float: "right"}} variant="light" onClick={() => addToOrders()}> {`𝗖𝗛𝗘𝗖𝗞𝗢𝗨𝗧`} </Button>
			<div className="m-2" style={{float: "right"}}> <h3>{`𝗚𝗿𝗮𝗻𝗱 𝗧𝗼𝘁𝗮𝗹: `} <span>&#8369;</span>{grandTotal} </h3>  </div>

		</Container>
		)
}