//[SECTION] React Components
	import { useState, useEffect, useContext } from 'react'
	import { Form } from 'react-bootstrap';
	import Swal from 'sweetalert2';
	import UserContext from '../UserContext';
	import { Navigate, useNavigate } from 'react-router-dom';
	import { BrowserRouter as Router } from 'react-router-dom';


//[SECTION] Material UI Components
	import * as React from 'react';
	import Avatar from '@mui/material/Avatar';
	import Button from '@mui/material/Button';
	import CssBaseline from '@mui/material/CssBaseline';
	import TextField from '@mui/material/TextField';
	import FormControlLabel from '@mui/material/FormControlLabel';
	import Checkbox from '@mui/material/Checkbox';
	import Link from '@mui/material/Link';
	import Grid from '@mui/material/Grid';
	import Box from '@mui/material/Box';
	import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
	import Typography from '@mui/material/Typography';
	import Container from '@mui/material/Container';
	import { createTheme, ThemeProvider } from '@mui/material/styles';
	

export default function Login() {
	const navigate = useNavigate();

	//Deconstruct the User Context object and its properties to use for user validation and to get the email coming from the login.
	const { user, setUser } = useContext(UserContext);

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	//BUTTON
	const [ isActive, setIsActive ] = useState(true);

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

	function authentication(e){
		e.preventDefault();

		fetch('https://emporium-global.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				phoneNumber: '',
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			//console.log(data)
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({
					accessToken: data.accessToken
				})

				Swal.fire({
					title: 'Success',
					icon: 'success',
				  	text: 'You are now logged in. Happy shopping!', 
				})

				//Get user's details from our token
				fetch('https://emporium-global.herokuapp.com/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					//console.log(data)

					if(data.isAdmin === true) {
						localStorage.setItem('isAdmin', data.isAdmin)

						setUser({
							isAdmin: data.isAdmin
						})

						Swal.fire({
							title: 'Success',
							icon: 'success',
						  	text: 'Dear admin, you are now logged in.', 
						})

						//Push to the /adminDashboard
						navigate('/products')

					} else {
						//If not an admin, push to the '/' (homepage)
						navigate('/')
					}

				})

			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Something went wrong. Check your email or password.'
				})
			}

			setEmail('');
			setPassword('');
		})
	}

	const theme = createTheme();

	try{
		return (
		<ThemeProvider theme={theme}>
		      <Container component="main" maxWidth="xs">
		        <CssBaseline />
		        <Box
		          sx={{
		            marginTop: 8,
		            display: 'flex',
		            flexDirection: 'column',
		            alignItems: 'center',
		          }}
		        >
		          <Avatar sx={{ m: 1, bgcolor: '#E8998D' }}>
		            <LockOutlinedIcon />
		          </Avatar>
		          <Typography component="h1" variant="h5">
		            Login
		          </Typography>
		          <Box component="form" onSubmit={e => authentication(e)} noValidate sx={{ mt: 1 }}>
		            <TextField
		              margin="normal"
		              required
		              fullWidth
		              id="email"
		              label="Email Address"
		              name="email"
		              autoComplete="email"
		              autoFocus
		              value={email} 
		              onChange={e => setEmail(e.target.value)}
		            />

		            <TextField
		              margin="normal"
		              required
		              fullWidth
		              name="password"
		              label="Password"
		              type="password"
		              id="password"
		              autoComplete="current-password"
		              value={password} 
		              onChange={e => setPassword(e.target.value)}
		            />

		            <FormControlLabel
		              control={<Checkbox value="remember" color="primary" />}
		              label="Remember me"
		            />

		            <Button
		              color="secondary"
		              type="submit"
		              fullWidth
		              variant="contained"
		              sx={{ mt: 3, mb: 2, bgcolor: '#5E548E', color: '#fff' }}
		            >
		              Login
		            </Button>

		            <Grid container>
		              <Grid item xs>
		                <Link href="#" variant="body2">
		                  Forgot password?
		                </Link>
		              </Grid>
		              <Grid item>
			                <Link href="#" variant="body2">
			                  {"Don't have an account? Sign Up"}
			                </Link>
		              </Grid>
		            </Grid>
		          </Box>
		        </Box>
		      </Container>
		    </ThemeProvider>
		);
  } catch(error){
    console.log(error)
  }
}
