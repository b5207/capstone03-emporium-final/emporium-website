//[SECTION] React Components
	import { useState, useContext, useEffect } from 'react';
	import { Container, Card, Button } from 'react-bootstrap';
	import UserContext from '../UserContext';
	import { useParams, Link, useNavigate } from 'react-router-dom';
	import Swal from 'sweetalert2';
	import { BsArrowLeft } from "react-icons/bs";
	import Cart from './Cart';
	import Styles from '../Styles.css';


export default function SpecificProduct() {

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [subTotal, setSubTotal] = useState(0);
	const [cart, setCart] = useState([]);
	const [image, setImage] = useState('');


	useEffect(() => {
		fetch(`https://emporium-global.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setImage(data.image)
		})
		setSubTotal(quantity * price)
	}, [subTotal, quantity])


	const { user } = useContext(UserContext);

	function decreaseQuantity () {
		setQuantity(prevQuantity => prevQuantity - 1)
		if(quantity === 0) {
			setQuantity(0)
		}
	}

	function increaseQuantity () {
		setQuantity(prevQuantity => prevQuantity + 1)
	}


	const addToCart = (itemId, qty, partialTotal) => {
		if (qty === 0) {
			Swal.fire({
				title: 'Product quantity should be at least 1.',
				icon: 'error'
			})
		} else {
			//console.log(partialTotal)
			//console.log(subTotal)
			//new data every time addToCart is invoked
			let newCartItem = {
				productId: itemId,
				name: name,
				price: price,
				quantity: qty,
				subTotal: partialTotal
			}
			//console.log(newCartItem)
			//save an empty array if nothing is stored yet
			if(localStorage.getItem('cartitems') == null){
				localStorage.setItem('cartitems', '[]')
			}
			//gets previously stored data if there's any
			let storedData = JSON.parse(localStorage.getItem('cartitems'));

			let newCartArray = [];
			let isExisting = false;

			for (let i = 0; i < storedData.length ; i++) {
				if (storedData[i].productId === itemId){
					storedData[i].quantity += qty;
					storedData[i].subTotal += partialTotal;
					isExisting = true;
				}
				newCartArray.push(storedData[i])
			}

			if (isExisting === false) {
				newCartArray.push(newCartItem)
			}

			localStorage.setItem('cartitems', JSON.stringify(newCartArray));
			
			Swal.fire({
			  position: 'top-end',
			  icon: 'success',
			  title: `Successfully added ${name} to your cart!`,
			  showConfirmButton: false,
			  timer: 1500
			})

		}
	}


	return(
		<Container>
			<Card>
				<Card.Header>
					<h4 className="text-center" style={{color: "#5E548E"}}>{ name }</h4>
				</Card.Header>

				<Card.Img className="w-25" variant="top" src={image}
				/>

				<Card.Body>
                  	<Card.Text>{ description }</Card.Text>
                  	<span><strong>Price:</strong></span>
                  	<span style={{color: "#5E548E"}}> <strong> <span>&#8369;</span>{ price } </strong></span>
				</Card.Body>

				<Card.Footer>
				{ user.accessToken !== null ?
					<>
					<span><strong>Quantity: </strong></span>
					<Button style={{backgroundColor: "#E8998D", color: "#fff"}} variant="light" onClick={decreaseQuantity}> - </Button>
							 <span> {` ${quantity} `} </span>
					<Button style={{backgroundColor: "#E8998D", color: "#fff"}} variant="light" onClick={increaseQuantity}> + </Button> 
					<h5 className="my-2">Subtotal: <span>&#8369;</span>{subTotal} </h5>
					<Button style={{backgroundColor: "#5E548E" , color: "#fff"}} className="ml-auto" variant="light" as={Link} to="/products"> {<BsArrowLeft />} </Button> 
					<Button style={{backgroundColor: "#5E548E" , color: "#fff"}} className="mx-2" variant="light" onClick={() => addToCart(productId, quantity, subTotal)}> Add to Cart </Button>
					<Button style={{backgroundColor: "#5E548E" , color: "#fff"}} variant="light" as={ Link } to={`/myCartView`}> View Cart</Button>
					</>
					:
					<Button style={{backgroundColor: "#5E548E" , color: "#fff"}} variant="light" as={ Link } to="/login"><h4>Login to Purchase</h4></Button>
				}
				</Card.Footer>
			</Card>
		</Container>

		)
}