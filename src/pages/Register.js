//[SECTION] React Components
  import { useState, useEffect, useContext } from 'react';
  import Swal from 'sweetalert2';
  import { Navigate, useNavigate } from 'react-router-dom';
  import UserContext from '../UserContext';

//[SECTION] Material UI Components
  import * as React from 'react';
  import Avatar from '@mui/material/Avatar';
  import Button from '@mui/material/Button';
  import CssBaseline from '@mui/material/CssBaseline';
  import TextField from '@mui/material/TextField';
  import FormControlLabel from '@mui/material/FormControlLabel';
  import Checkbox from '@mui/material/Checkbox';
  import Link from '@mui/material/Link';
  import Grid from '@mui/material/Grid';
  import Box from '@mui/material/Box';
  import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
  import Typography from '@mui/material/Typography';
  import Container from '@mui/material/Container';
  import { createTheme, ThemeProvider } from '@mui/material/styles';


export default function Register() {

      //THEME
      const theme = createTheme();

      const { user } = useContext(UserContext);
      const navigate = useNavigate();

      //[State hooks] to store the values of the input fields.
      const [ fullName, setFullName ] = useState('');
      const [ phoneNumber, setPhoneNumber ] = useState('');
      const [ email, setEmail ] = useState('');
      const [ password, setPassword ] = useState('');
      const [ verifyPassword, setVerifyPassword ] = useState('');

      //[State] for the enable/disable button
      const [ isActive, setIsActive ] = useState(true);


      useEffect(() => {
        //Validation to enable submit button.
        if((fullName !=='' && phoneNumber !== '' && email !== '' && password !=='' && verifyPassword !=='') && (password === verifyPassword)){
          setIsActive(true);
        } else {
          setIsActive(false);
        }
      }, [fullName, phoneNumber, email, password, verifyPassword])


  //FUNCTION REGISTER-USER
      function registerUser(e) {

        e.preventDefault();

        fetch('https://emporium-global.herokuapp.com/users/register', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'}, 
            body: JSON.stringify({
                fullName: fullName,
                phoneNumber: phoneNumber,
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

          if(data !== false){
            //Clear input fields
            setFullName('');
            setEmail('');
            setPhoneNumber('');
            setPassword('');
            setVerifyPassword('');

            navigate('/login')

            Swal.fire({
              title: 'Success',
              icon: 'success',
              text: 'You have successfully registered a new account. Please login.'
            })

          } else {
            Swal.fire({
              title: 'Error',
              icon: 'error',
              text: 'You have created a new account!'
            })
          }
        })
      }
      
  try{
    return (
      (user.accessToken !== null) ?
      <Navigate to="/products" />
      :

      <ThemeProvider theme={theme}>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: '#E8998D' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign Up
            </Typography>
            <Box component="form" noValidate onSubmit={e => registerUser(e)} sx={{ mt: 3 }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                      <TextField
                        autoComplete="given-name"
                        name="fullName"
                        required
                        fullWidth
                        id="fullName"
                        label="Full Name"
                        autoFocus
                        value={fullName}
                        onChange={e => setFullName(e.target.value)}
                      />
                </Grid>

                <Grid item xs={12}>
                      <TextField
                        name="phoneNumber"
                        required
                        fullWidth
                        id="phoneNumber"
                        label="Phone Number"
                        autoFocus
                        value={phoneNumber}
                        onChange={e => setPhoneNumber(e.target.value)}
                      />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    value={email} 
                    onChange={e => setEmail(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="new-password"
                    value={password} 
                    onChange={e => setPassword(e.target.value)}
                  />
                </Grid>
                <Grid item xs={12}>
                      <TextField
                        required
                        fullWidth
                        name="verifyPassword"
                        label="Verify Password"
                        type="password"
                        id="verifyPassword"
                        autoComplete="new-password"
                        value={verifyPassword} 
                        onChange={e => setVerifyPassword(e.target.value)}
                      />
                </Grid>
                <Grid item xs={12}>
                  <FormControlLabel
                    control={<Checkbox value="allowExtraEmails" color="primary" />}
                    label="I want to receive inspiration, marketing promotions and updates via email."
                  />
                </Grid>
              </Grid>
              <Button
                  color="secondary"
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2, bgcolor: '#5E548E', color: '#fff' }}
              >
                Sign Up
              </Button>
              <Grid container justifyContent="flex-end">
                <Grid item>
                  <Link href="#" variant="body2">
                    Already have an account? Sign in
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Container>
      </ThemeProvider>
    );
  } catch(error){
    console.log(error)
  }
}